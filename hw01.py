#!/usr/bin/env python
# Author: Ondrej Platek 2012; contact:  ondrej.platek at seznam.cz
# Code is provided as is with no warranty. Use it on your own risk.
# Use the source code according GPL3 and distribute the code also with this 3 line header
from dotplot import *

def generatePlots(windows,dna_name):
    with open(dna_name,'rb') as f:
        dna=f.read()
        m=dotplotArr(dna,dna)
        display_PIL(m,'%s_dotplot-basic.png'%dna_name)
        for win_size,t in windows:
            wm=dotplotSlideWinArr(m,win_size, t)
            display_PIL(wm,'%s_dotplot-win%d_t_%d.png'%(dna_name,win_size,t))


def task3():
    dna_name='DNA-sample1.txt'
    windows=[(2,0),(2,1),(2,2),(2,3),(2,4),
            (3,1),(3,3),(3,6),
            (4,1),(4,4),(4,8),
            (8,4),(8,16),(8,32),
            (16,8),(16,64),(16,128)]
    generatePlots(windows,dna_name)

def task4():
    dna_name='DNA-nm_000044-homo-sapiens.txt'
    windows=[(25,1),(25,5),(25,10),(25,20),(25,30),(25,50),(25,80),(25,150),(25,200)]
    generatePlots(windows,dna_name)

def main():
    task3()
#    task4()

if __name__=='__main__':
    main()
