#!/usr/bin/env python
# Author: Ondrej Platek 2012; contact:  ondrej.platek at seznam.cz
# Code is provided as is with no warranty. Use it on your own risk.
# Use the source code according GPL3 and distribute the code also with this 3 line header
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
mpl.use('Agg')
import pylab
import matplotlib.cm as cm
#
def plotMatplot(m,name=None):
    minm=np.min(m)
    maxm=np.max(m)
    fig=plt.figure()
    ax=fig.add_subplot(111)
    cax=ax.imshow(m,interpolation='nearest',cmap=cm.gist_gray)
    cbar = fig.colorbar(cax, ticks=[minm,maxm])
    cbar.ax.set_yticklabels([str(minm),str(maxm)])
    if name==None:
        plt.show()
        print 'Figure drawned.'
    else:
        plt.savefig(name)
        print 'Figure %s saved ' % name
